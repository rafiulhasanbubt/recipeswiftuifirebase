//
//  RecipeSwiftUIFirebaseApp.swift
//  RecipeSwiftUIFirebase
//
//  Created by rafiul hasan on 19/11/21.
//

import SwiftUI
import Firebase

@main
struct RecipeSwiftUIFirebaseApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    var body: some Scene {
        WindowGroup {
            SignupView()
            //ContentView()
        }
    }
}

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        func setupFirebase() {
          FirebaseConfiguration.shared.setLoggerLevel(.min)
          FirebaseApp.configure()
        }
        
        //FirebaseApp.configure()
        print("Firebase connected successfully")
        return true
    }
}
