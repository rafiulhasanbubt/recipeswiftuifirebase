//
//  StorageService.swift
//  RecipeSwiftUIFirebase
//
//  Created by rafiul hasan on 26/11/21.
//

import Foundation
import Firebase
import FirebaseStorage

class StorageService {
    static var storage = Storage.storage()
    static var storageRoot = storage.reference(forURL: "gs://recipeswiftuifirebase.appspot.com")
    static var storageProfile = storageRoot.child("profileImages")
    
    static func storageProfileId(userId: String) -> StorageReference {
        return storageProfile.child(userId)
    }
    
    //MARK: Save profile image
    static func saveProfileImage(userId: String, firstname: String, lastname: String, username: String, address: String, email: String, imageData: Data, metaData: StorageMetadata, storageProfileImageRef: StorageReference, onSuccess: @escaping(_ user: User) -> Void, onError: @escaping(_ errorMessage: String) -> Void) {
        
        storageProfileImageRef.putData(imageData, metadata: metaData) { storageMetadata, error in
            
            if error != nil {
                onError(error!.localizedDescription)
                return
            }
            
            storageProfileImageRef.downloadURL { url, error in
                if let metaImageUrl = url?.absoluteString {
                    if let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest() {
                        changeRequest.photoURL = url
                        changeRequest.displayName = username
                        changeRequest.commitChanges { error in
                            if error != nil {
                                onError(error!.localizedDescription)
                                return
                            }
                        }
                    }
                    
                    let firestorageUserId = AuthService.getUserId(userId: userId)
                    let user = User.init(uid: userId, firstname: firstname, lastname: lastname, username: username, address: address, email: email, profileImageUrl: metaImageUrl)
                    
                    guard let dict = try? user.asDictionary() else { return }
                    
                    firestorageUserId.setData(dict) { error in
                        if error != nil {
                            onError(error!.localizedDescription)
                        }
                    }
                    onSuccess(user)
                }
            }
        }
    }
    
    //MARK: Edit profile
    static func editProfile(userId: String, username: String, bio: String, imageData: Data, metaData: StorageMetadata, storageProfileImageRef: StorageReference, onError: @escaping(_ error: String) -> Void) {
        
        storageProfileImageRef.putData(imageData, metadata: metaData) { storageMetadata, error in
            
            if error != nil {
                onError(error!.localizedDescription)
                return
            }
            
            storageProfileImageRef.downloadURL { url, error in
                if let metaImageUrl = url?.absoluteString {
                    if let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest() {
                        changeRequest.photoURL = url
                        changeRequest.displayName = username
                        changeRequest.commitChanges { error in
                            if error != nil {
                                onError(error!.localizedDescription)
                                return
                            }
                        }
                    }
                    
                    let firestorageUserId = AuthService.getUserId(userId: userId)
                    firestorageUserId.updateData(["profileImageUrl": metaImageUrl, "username":username, "bio": bio])
                }
            }
        }
    }
    
}
