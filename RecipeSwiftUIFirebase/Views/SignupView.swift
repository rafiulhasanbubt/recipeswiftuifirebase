//
//  SignupView.swift
//  RecipeSwiftUIFirebase
//
//  Created by rafiul hasan on 26/11/21.
//

import SwiftUI

struct SignupView: View {
    @State private var firstName = ""
    @State private var lastName = ""
    @State private var userName = ""
    @State private var address = ""
    @State private var email = ""
    @State private var password = ""
    @State private var confirmPassword = ""
    
    @State private var profileImage: Image?
    @State private var pickedImage: Image?
    @State private var showingActionSheet = false
    @State private var showingImagePicker = false
    @State private var imageData: Data = Data()
    @State private var sourceType: UIImagePickerController.SourceType = .photoLibrary
    @State private var error: String = ""
    @State private var showingAlert = false
    @State private var alertTitle: String = ""
    @State private var showHomePage = false
    
    var body: some View {
        
        ZStack {
            Color.gray
                .ignoresSafeArea(.all)
            
            VStack(spacing: 16) {
                
                VStack {
                    Group {
                        if profileImage != nil {
                            profileImage!.resizable()
                                .clipShape(Circle())
                                .frame(width: 200, height: 200)
                                .padding(.top, 20)
                                .onTapGesture {
                                    self.showingActionSheet = true
                                }
                        } else {
                            Image(systemName: "person.circle.fill")
                                .resizable()
                                .clipShape(Circle())
                                .frame(width: 200, height: 200)
                                .padding(.top, 20)
                                .onTapGesture {
                                    self.showingActionSheet = true
                                }
                        }
                    }
                }
                
                TextField("First Name", text: $firstName)
                    .textFieldStyle(RoundedBorderTextFieldStyle.roundedBorder)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                
                TextField("Last Name", text: $lastName)
                    .textFieldStyle(RoundedBorderTextFieldStyle.roundedBorder)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                TextField("user Name", text: $userName)
                    .textFieldStyle(RoundedBorderTextFieldStyle.roundedBorder)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                TextField("Address", text: $address)
                    .textFieldStyle(RoundedBorderTextFieldStyle.roundedBorder)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                TextField("Email", text: $email)
                    .textFieldStyle(RoundedBorderTextFieldStyle.roundedBorder)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                
                TextField("Password", text: $password)
                    .textFieldStyle(RoundedBorderTextFieldStyle.roundedBorder)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                
                TextField("Confirm password", text: $confirmPassword)
                    .textFieldStyle(RoundedBorderTextFieldStyle.roundedBorder)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                
                Button(action: {
                    signUp()
                }) {
                    Text("Sign up")
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .frame(height: 20)
                        .padding()
                        .foregroundColor(.white)
                        .font(.system(size: 20, weight: .bold))
                        .background(Color.blue)
                        .cornerRadius(5)
                }
                
                Button(action: {
                    
                }) {
                    Text("Login")
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .frame(height: 20)
                        .padding()
                        .foregroundColor(.white)
                        .font(.system(size: 14, weight: .bold))
                        .background(Color.blue)
                        .cornerRadius(5)
                }
            }
            .padding()
            .sheet(isPresented: $showingImagePicker, onDismiss: loadImage) {
                ImagePicker(pickedImage: self.$pickedImage,
                            showImagePicker: self.$showingImagePicker,
                            imageData: self.$imageData)
            }.actionSheet(isPresented: $showingActionSheet) {
                ActionSheet(title: Text(""), buttons: [
                    .default(Text("Choose a Photo")){
                        self.sourceType = .photoLibrary
                        self.showingImagePicker = true
                    },
                    .default(Text("Take a Photo")){
                        self.sourceType = .camera
                        self.showingImagePicker = true
                    },
                    .cancel()
                ])
            }
        }
    }
    
    //MARK: functions
    func loadImage() {
        guard let inputImage = pickedImage else { return }
        profileImage = inputImage
    }
    
    func signUp() {
//            if let error = errorCheck() {
//                self.error = error
//                self.showingAlert = true
//                self.clear()
//                return
//            }
        AuthService.signUp(firstname: firstName, lastname: lastName, username: userName, address: address, email: email, password: password, imageData: imageData) { user in
            print(user)
            HomeView()
        } onError: { errorMessage in
            print(errorMessage)
        }

//            AuthService.signUp(username: username, email: email, password: password, imageData: imageData, onSuccess: { user in
//                self.clear()
//                HomeView()
//            }) { errorMessage in
//                print("Error \(errorMessage)")
//                self.error = errorMessage
//                self.showingAlert = true
//                return
//            }
        }
}

struct SignupView_Previews: PreviewProvider {
    static var previews: some View {
        SignupView()
    }
}
