//
//  UserModel.swift
//  RecipeSwiftUIFirebase
//
//  Created by rafiul hasan on 26/11/21.
//

import Foundation

struct User: Encodable, Decodable {
    var uid: String
    var firstname: String
    var lastname: String
    var username: String
    var address: String
    var email: String
    var profileImageUrl: String
}
